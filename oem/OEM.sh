#!/bin/sh
#
# Thunderbird automatic install for Linux - OEM - Silent instal.
# v1.1.0
#
# This OEM file (oem.sh) is redundant. You do not absolutely need to use this file. 
# The enclosed setup files (for example, thunderbird_nightly.sh) within the OEM folder (directory) can work independently.
# However, to use this file anyways, uncomment what you need and save.

# When done comment out the text below.
printf " If you are currently seeing this TEXT, your developer has not configured Thunderbird Automatic Install for Linux correctly as they should have commented out this text.";
# When done comment out the text above.

# Mozilla Thunderbird
# chmod +x ./thunderbird_stable.sh ; bash ./thunderbird_stable.sh ;

# Mozilla Thunderbird Beta
# chmod +x ./thunderbird_beta.sh ; bash ./thunderbird_beta.sh ;

# Mozilla Thunderbird Daily
# chmod +x ./thunderbird_daily.sh ; bash ./thunderbird_daily.sh ;

# Exit
exit 0
