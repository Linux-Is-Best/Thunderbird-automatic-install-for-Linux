# Thunderbird automatic install for Linux

Automated installation for Mozilla Thunderbird for The Linux Operating System.

## Project objective

The objective is to provide a method to easily install Mozilla Thunderbird directly from Mozilla's website and enable Thunderbird's automatic update feature for the latest releases. Providing a pure stock Mozilla Thunderbird experience for everyone using your Linux computer at home. 

## Getting started

1. [**Download** the latest release](https://gitlab.com/Linux-Is-Best/Thunderbird-automatic-install-for-Linux/-/releases)
1. Extract the archive.
1. Open a terminal window inside the newly extracted folder (directory).
1. Follow the directions below.

In the terminal window run the following command:

```bash
chmod +x Setup.sh # Or for 32-bit installs: chmod +x Setup32.sh
```

This will make the script executable (allowing it to run).  Next, execute the script by typing the following.

```bash
bash Setup.sh # Or for 32-bit installs: bach Setup32.sh
```
   
   (Or your perfered shell followed by Setup.sh)

Please follow the easy menu prompts and complete the setup. You can install or uninstall any edition you wish, including multiple editions side by side.

   Enjoy your e-mail!


## Extra

The OEM directory (folder) assumes you are already an advanced user with root or chroot privileges, installing Thunderbird system-wide for all users. Furthermore, the OEM installs function as a silent installation method.

## Firefox Automatic Install for Linux

If you like Thunderbird Automatic Install for Linux, you may also like, [Firefox Automatic Install for Linux](https://gitlab.com/Linux-Is-Best/Firefox-automatic-install-for-Linux).
