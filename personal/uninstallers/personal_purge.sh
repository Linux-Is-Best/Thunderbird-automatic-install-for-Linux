#!/bin/sh
#
# Thunderbird automatic install for Linux - personal uninstall ALL - 64-bit
# v1.0.0

clear;
printf '\n As per your request, I will now PURGE Mozilla Thunderbird.\n'
printf '\n This action uninstalls ALL personal copies of Mozilla Thunderbird and remove ALL user cache, cookies, and profiles.\n'

## Install
rm -r -f  "$HOME"/Mozilla/thunderbird/ ;
rm -r -f  "$HOME"/Mozilla/thunderbird_beta/ ;
rm -r -f  "$HOME"/Mozilla/thunderbird_daily/ ;

# Menu shortcuts
rm -r -f "$HOME"/.local/share/applications/Mozilla_Thunderbird.desktop ;
rm -r -f "$HOME"/.local/share/applications/Mozilla_Thunderbird_Beta.desktop ;
rm -r -f "$HOME"/.local/share/applications/Mozilla_Thunderbird_Daily.desktop ;

# Desktop shotcuts

# determine the XDG_DESKTOP_DIR, for multi language support!
DESKTOP_DIR=$(xdg-user-dir DESKTOP)
# Current desktop shortcuts
rm -r -f "$DESKTOP_DIR"/Mozilla_Thunderbird.desktop ;
rm -r -f "$DESKTOP_DIR"/Mozilla_Thunderbird_Beta.desktop ;
rm -r -f "$DESKTOP_DIR"/Mozilla_Thunderbird_Daily.desktop ;

## PURGE - Everything go, bye-bye.

# To delete file cache.
rm -r -f "$HOME"/.cache/thunderbird/

# To delete configuration and profile files.
rm -r -f "$HOME"/.thunderbird/

printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird." \
" Mozilla Thunderbird has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# Exit
exit 0
