#!/bin/sh
#
# Thunderbird automatic install for Linux - personal uninstall Thunderbird - 64-bit

# Prompt for user confirmation
printf "Are you sure you want to uninstall Thunderbird? [y/N]: "
read -r confirm

if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
    printf -- '%s\n' "" "" "Uninstallation canceled. Thunderbird has not been deleted." "" ""
    exit 0
fi

# Uninstall notice
printf -- '\n%s\n' " Uninstalling your personal copy of Mozilla Thunderbird.";

## Install
rm -r -f  /home/"$USER"/Mozilla/thunderbird/ ;

# Menu shortcuts
rm -r -f /home/"$USER"/.local/share/applications/Mozilla_Thunderbird.desktop ;

# Desktop shotcuts

# determine the XDG_DESKTOP_DIR, for multi language support!
DESKTOP_DIR=$(xdg-user-dir DESKTOP)
# Current desktop shortcuts
rm -r -f "$DESKTOP_DIR"/Mozilla_Thunderbird.desktop ;

printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird." \
" Mozilla Thunderbird has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# Exit
exit 0
