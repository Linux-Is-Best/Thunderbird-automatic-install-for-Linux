#!/bin/sh
#
# Thunderbird automatic install for Linux - all personal 64-bit
# v1.1.0
#
# This script installs all 64-bit releases of Mozilla Thunderbird.

# Specify the directory where the Thunderbird installation scripts are located
SCRIPT_DIR="./personal"

# Set executable permissions for all files in the ./personal directory
chmod -R +x ./personal

# List of edition names
EDITIONS="Stable Beta Daily"

# Function to install all editions
install_all_editions() {
  printf -- '\n%s\n' "Now installing all versions of Mozilla Thunderbird. This may take a moment, please wait..."
  sleep 2

  for edition in $EDITIONS; do
    script_path="${SCRIPT_DIR}/Thunderbird_${edition}.sh"
    if [ -f "$script_path" ]; then
      if [ -x "$script_path" ]; then
        "$script_path"
      else
        printf -- '\n%s\n' "Script not executable: $script_path"
      fi
    else
      printf -- '\n%s\n' "Script not found: $script_path"
    fi
  done
}

# Display an exit notice
display_exit_notice() {
  printf -- '%s\n' "" "" "" "Congratulations!" \
    "All versions of Mozilla Thunderbird have been successfully installed on your computer." \
    "They will update automatically, so no additional action is required." \
    "Enjoy your browsing experience!" "" ""
}

# Main script execution
install_all_editions
display_exit_notice

# Exit
exit 0
