#!/bin/sh
#
# Thunderbird Automatic Install for Linux - Thunderbird (current stable edition) - personal
#

## SUSE Patch -- Will safely skip if not using a SUSE based distribution and continue with normal install of Thunderbird. ##

# SUSE Linux stands out as an exceptional distribution with one notable absence: it lacks a crucial, free, and fundamental library that
# is typically present in most mainstream distributions. As a consequence, extracting a standard TAR.BZ2 file becomes impossible 
# without this essential library. For instance, the straightforward command 'sudo tar xjf file_name.tar.bz2 -C /opt/path_name/' would
# fail to execute. It might be worthwhile to encourage SUSE to reconsider their stance and include this library to alleviate such inconveniences.

# Function to get the value of a specific key from /etc/os-release
get_os_release_value() {
  key="$1"
  awk -F= -v k="$key" '$1 == k {gsub("\"", "", $2); print $2}' /etc/os-release
}

# Check if the OS is SUSE or openSUSE
if [ -f "/etc/os-release" ]; then
  ID="$(get_os_release_value ID)"
  if [ "$ID" = "suse" ] || [ "$ID" = "opensuse" ]; then
    # Check if bzip2 is installed
    if ! command -v bzip2 >/dev/null 2>&1; then
printf "Hello there!\n\n"
printf "We want to ensure you have the best experience with SUSE Linux.\n"
printf "Typically, all mainstream Linux distributions come with a free library that makes working with TAR.BZ2 archives a breeze.\n\n"
printf "However, in SUSE Linux, this library is not included by default.\n"
printf "But don't worry, we've got you covered!\n\n"
printf "To work with standard TAR.BZ2 archives in SUSE, simply install the 'bzip2' package by following these simple steps:\n"
printf "1. Contact your system administrator (if you're not one yourself).\n"
printf "2. Ask them to install the 'bzip2' package on your system.\n"
printf "Once that's done, you'll be able to handle TAR.BZ2 files without any hassle.\n\n"
printf "If you find this library essential and believe it would benefit other SUSE users as well, consider letting SUSE know!\n"
printf "You can reach out to their support team or visit their website to share your feedback.\n"
printf "User input is valuable, and feedback often drives positive changes in future releases.\n\n"
printf "We're here to help you make the most out of your SUSE experience!\n"
      exit 1
    fi
  fi
fi

## End SUSE Patch -- We now return to our regular scheduled programming. ##

# Function to display the error message
display_error_message() {
  printf -- '\n%s\n' "" "" "" "Oops, something went wrong!" \
    "I apologize, but I couldn't download Thunderbird from Mozilla." \
    "There are a few possible reasons for this:" \
    "  - It's possible that either 'curl' or 'wget' is not available on your computer." \
    "  - Your internet connection might be experiencing issues." \
    "Please take the following steps to troubleshoot the problem:" \
    "  - If 'curl' or 'wget' is not installed, please install either of them (either will work)." \
    "  - Check your internet connection and ensure it's working properly." \
    "If you need further assistance, feel free to contact your system administrator." \
    "" " " \
    "I'm now exiting the install process. Nothing has changed on your computer." "" ""

  # Error exit.
  exit 1
}

# Download notice.
printf -- '\n%s\n' "
Please wait. I am downloading the latest version of Mozilla Thunderbird.
"

# 4-second wait before beginning download. Gives the user time to read the above sentence and understand what is happening.
sleep 4

# Define the file to download
url="https://download.mozilla.org/?product=thunderbird-latest-ssl&os=linux64"
file="ThunderbirdStable.tar.bz2"

# Function to download with wget or curl, depending on availability
download_with_wget_or_curl() {
  if command -v wget >/dev/null 2>&1; then
    wget -L --show-progress -O "$file" "$url" || return 1
  elif command -v curl >/dev/null 2>&1; then
    curl -# -L -o "$file" "$url" || return 1
  else
    return 1
  fi
}

# Attempt to download with wget or curl
if ! download_with_wget_or_curl; then
  display_error_message
fi

# Begin install notice.
printf -- '\n\n\n%s\n\n' "
Installing Mozilla Thunderbird.
"

# Create /Mozilla directory in /home/$USER if it doesn't exist
mkdir -p "$HOME"/Mozilla ;

# Set permissions for
chmod 755 "$HOME"/Mozilla ;

# Extracts to install path
tar xjf "$file" -C "$HOME"/Mozilla ;

# Required permissions needed for Mozilla Thunderbird automatic update feature to work.
chmod -R 757 "$HOME"/Mozilla/thunderbird/ ;

# Create icon script using echo command
printf "[Desktop Entry]
Exec=$HOME/Mozilla/thunderbird/thunderbird %%u --class MozillaThunderbird
Icon=$HOME/Mozilla/thunderbird/chrome/icons/default/default256.png
Terminal=false
Type=Application
Categories=Network;Email;
MimeType=message/rfc822;x-scheme-handler/mailto;application/x-xpinstall;
StartupNotify=true
Actions=ComposeMessage;OpenAddressBook;
Name=Mozilla Thunderbird
Comment=Send and receive mail with Thunderbird
Comment[ast]=Lleer y escribir corréu electrónicu
Comment[ca]=Llegiu i escriviu correu
Comment[cs]=Čtení a psaní pošty
Comment[da]=Skriv/læs e-post/nyhedsgruppe med Mozilla Thunderbird
Comment[de]=E-Mails und Nachrichten mit Thunderbird lesen und schreiben
Comment[el]=Διαβάστε και γράψτε γράμματα με το Mozilla Thunderbird
Comment[es]=Lea y escriba correos y noticias con Thunderbird
Comment[fi]=Lue ja kirjoita sähköposteja
Comment[fr]=Lire et écrire des courriels
Comment[gl]=Lea e escriba correo electrónico
Comment[he]=קריאה/כתיבה של דוא״ל/חדשות באמצעות Mozilla Thunderbird
Comment[hr]=Čitajte/šaljite e-poštu s Thunderbird
Comment[hu]=Levelek írása és olvasása a Thunderbirddel
Comment[it]=Per leggere e scrivere email
Comment[ja]=メールの読み書き
Comment[ko]=Mozilla Thunderbird 메일/뉴스 읽기 및 쓰기 클라이언트
Comment[nl]=E-mail/nieuws lezen en schrijven met Mozilla Thunderbird
Comment[pl]=Czytanie i wysyłanie e-maili
Comment[pt_BR]=Leia e escreva suas mensagens
Comment[ru]=Читайте и пишите письма
Comment[sk]=Čítajte a píšte poštu pomocou programu Thunderbird
Comment[sv]=Läs och skriv e-post
Comment[ug]=ئېلخەت ۋە خەۋەرلەرنى Mozilla Thunderbird دا كۆرۈش ۋە يېزىش
Comment[uk]=Читання та написання листів
Comment[vi]=Đọc và soạn thư điện tử
Comment[zh_CN]=阅读邮件或新闻
Comment[zh_TW]=以 Mozilla Thunderbird 讀寫郵件或新聞
GenericName=Mail Client
GenericName[ast]=Client de correu
GenericName[ca]=Client de correu
GenericName[cs]=Poštovní klient
GenericName[da]=E-postklient
GenericName[de]=E-Mail-Anwendung
GenericName[el]=Λογισμικό αλληλογραφίας
GenericName[es]=Cliente de correo
GenericName[fi]=Sähköpostiohjelma
GenericName[fr]=Client de messagerie
GenericName[gl]=Cliente de correo electrónico
GenericName[he]=לקוח דוא״ל
GenericName[hr]=Klijent e-pošte
GenericName[hu]=Levelezőkliens
GenericName[it]=Client email
GenericName[ja]=電子メールクライアント
GenericName[ko]=메일 클라이언트
GenericName[nl]=E-mailprogramma
GenericName[pl]=Klient poczty
GenericName[pt_BR]=Cliente de E-mail
GenericName[ru]=Почтовый клиент
GenericName[sk]=Poštový klient
GenericName[ug]=ئېلخەت دېتالى
GenericName[uk]=Поштова програма
GenericName[vi]=Phần mềm khách quản lý thư điện tử
GenericName[zh_CN]=邮件新闻客户端
GenericName[zh_TW]=郵件用戶端

[Desktop Action ComposeMessage]
Exec=$HOME/Mozilla/thunderbird/thunderbird -compose %%u --class MozillaThunderbird
Name=Write new message
Name[ar]=اكتب رسالة جديدة
Name[ast]=Redactar mensaxe nuevu
Name[be]=Напісаць новы ліст
Name[bg]=Съставяне на ново съобщение
Name[br]=Skrivañ ur gemennadenn nevez
Name[ca]=Escriu un missatge nou
Name[cs]=Napsat novou zprávu
Name[da]=Skriv en ny meddelelse
Name[de]=Neue Nachricht verfassen
Name[el]=Σύνταξη νέου μηνύματος
Name[es_AR]=Escribir un nuevo mensaje
Name[es_ES]=Redactar nuevo mensaje
Name[et]=Kirjuta uus kiri
Name[eu]=Idatzi mezu berria
Name[fi]=Kirjoita uusi viesti
Name[fr]=Rédiger un nouveau message
Name[fy_NL]=Skriuw in nij berjocht
Name[ga_IE]=Scríobh teachtaireacht nua
Name[gd]=Sgrìobh teachdaireachd ùr
Name[gl]=Escribir unha nova mensaxe
Name[he]=כתיבת הודעה חדשה
Name[hr]=Piši novu poruku
Name[hu]=Új üzenet írása
Name[hy_AM]=Գրել նոր նամակ
Name[is]=SKrifa nýjan póst
Name[it]=Scrivi nuovo messaggio
Name[ja]=新しいメッセージを作成する
Name[ko]=새 메시지 작성
Name[lt]=Rašyti naują laišką
Name[nb_NO]=Skriv ny melding
Name[nl]=Nieuw bericht aanmaken
Name[nn_NO]=Skriv ny melding
Name[pl]=Nowa wiadomość
Name[pt_BR]=Nova mensagem
Name[pt_PT]=Escrever nova mensagem
Name[rm]=Scriver in nov messadi
Name[ro]=Scrie un mesaj nou
Name[ru]=Создать новое сообщение
Name[si]=නව ලිපියක් ලියන්න
Name[sk]=Nová e-mailová správa
Name[sl]=Sestavi novo sporočilo
Name[sq]=Shkruani mesazh të ri
Name[sr]=Писање нове поруке
Name[sv_SE]=Skriv ett nytt meddelande
Name[ta_LK]=புதிய செய்தியை எழுதுக
Name[tr]=Yeni ileti yaz
Name[uk]=Написати нового листа
Name[vi]=Viết thư mới
Name[zh_CN]=编写新消息
Name[zh_TW]=寫一封新訊息

[Desktop Action OpenAddressBook]
Exec=$HOME/Mozilla/thunderbird/thunderbird -addressbook %%u --class MozillaThunderbird
Name=Open address book
Name[ar]=افتح دفتر العناوين
Name[ast]=Abrir llibreta de direiciones
Name[be]=Адкрыць адрасную кнігу
Name[bg]=Отваряне на адресник
Name[br]=Digeriñ ur c'harned chomlec'hioù
Name[ca]=Obre la llibreta d'adreces
Name[cs]=Otevřít Adresář
Name[da]=Åbn adressebog
Name[de]=Adressbuch öffnen
Name[el]=Άνοιγμα ευρετηρίου διευθύνσεων
Name[es_AR]=Abrir libreta de direcciones
Name[es_ES]=Abrir libreta de direcciones
Name[et]=Ava aadressiraamat
Name[eu]=Ireki helbide-liburua
Name[fi]=Avaa osoitekirja
Name[fr]=Ouvrir un carnet d'adresses
Name[fy_NL]=Iepenje adresboek
Name[ga_IE]=Oscail leabhar seoltaí
Name[gd]=Fosgail leabhar-sheòlaidhean
Name[gl]=Abrir a axenda de enderezos
Name[he]=פתיחת ספר כתובות
Name[hr]=Otvori adresar
Name[hu]=Címjegyzék megnyitása
Name[hy_AM]=Բացել Հասցեագիրքը
Name[is]=Opna nafnaskrá
Name[it]=Apri rubrica
Name[ja]=アドレス帳を開く
Name[ko]=주소록 열기
Name[lt]=Atverti adresų knygą
Name[nb_NO]=Åpne adressebok
Name[nl]=Adresboek openen
Name[nn_NO]=Opne adressebok
Name[pl]=Książka adresowa
Name[pt_BR]=Catálogo de endereços
Name[pt_PT]=Abrir livro de endereços
Name[rm]=Avrir il cudeschet d'adressas
Name[ro]=Deschide agenda de contacte
Name[ru]=Открыть адресную книгу
Name[si]=ලිපින පොත විවෘත කරන්න
Name[sk]=Otvoriť adresár
Name[sl]=Odpri adressar
Name[sq]=Hapni libër adresash
Name[sr]=Отвори адресар
Name[sv_SE]=Öppna adressboken
Name[ta_LK]=முகவரி பத்தகத்தை திறக்க
Name[tr]=Adres defterini aç
Name[uk]=Відкрити адресну книгу
Name[vi]=Mở sổ địa chỉ
Name[zh_CN]=打开通讯录
Name[zh_TW]=開啟通訊錄 " > Mozilla_Thunderbird.desktop ;

# Give time for icon script to complete
sleep 2;

# Makes icon executable allowing it to run Thunderbird (which is also executable).
chmod +x Mozilla_Thunderbird.desktop ;

# Adds icon to application menu (xfce, gnome, cinnamon, mate, deepin, etc...).
cp Mozilla_Thunderbird.desktop "$HOME"/.local/share/applications/ ;

# determine the XDG_DESKTOP_DIR, for multi language support!
DESKTOP_DIR=$(xdg-user-dir DESKTOP)

# Copies desktop icon to all user desktops and grants them ownership (it is their desktop after all).
find "$DESKTOP_DIR" -maxdepth 1 -type d -exec cp Mozilla_Thunderbird.desktop '{}' \; -exec chown --reference='{}' '{}/Mozilla_Thunderbird.desktop' \;

# Removes the temporary files no longer needed.
rm "$file" ; rm Mozilla_Thunderbird.desktop ;

printf -- '%s\n' "" "" "🎉 Congratulations! 🎉" \
    "Mozilla Thunderbird has been successfully installed on your computer." \
    "From now on, Mozilla Thunderbird will handle its own updates automatically." \
    "Enjoy your e-mail experience with the latest features and improvements!" "" ""

# Exit
exit 0
