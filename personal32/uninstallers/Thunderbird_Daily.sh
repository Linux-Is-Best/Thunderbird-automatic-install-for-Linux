#!/bin/sh
#
# Thunderbird automatic install for Linux - personal uninstall Thunderbird Daily - 32-bit

# Prompt for user confirmation
printf "Are you sure you want to uninstall Thunderbird Daily 32-bit? [y/N]: "
read -r confirm

if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
    printf -- '%s\n' "" "" "Uninstallation canceled. Thunderbird Daily 32-bit has not been deleted." "" ""
    exit 0
fi

# Uninstall notice
printf -- '\n%s\n' " Uninstalling your personal copy of Mozilla Thunderbird Daily.";

## Install
rm -r -f  "$HOME"/Mozilla/thunderbird_daily32/ ;

# Menu shortcuts
rm -r -f "$HOME"/.local/share/applications/Mozilla_Thunderbird_Daily32.desktop ;

# Desktop shotcuts

# determine the XDG_DESKTOP_DIR, for multi language support!
DESKTOP_DIR=$(xdg-user-dir DESKTOP)
# Current desktop shortcuts
rm -r -f "$DESKTOP_DIR"/Mozilla_Thunderbird_Daily32.desktop ;

printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird Daily." \
" Mozilla Thunderbird Daily has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# Exit
exit 0
