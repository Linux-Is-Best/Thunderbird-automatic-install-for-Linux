#!/bin/sh
#
# Thunderbird automatic install for Linux - personal uninstall Thunderbird - 32-bit

# Prompt for user confirmation
printf "Are you sure you want to uninstall Thunderbird 32-bit? [y/N]: "
read -r confirm

if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
    printf -- '%s\n' "" "" "Uninstallation canceled. Thunderbird 32-bit has not been deleted." "" ""
    exit 0
fi

# Uninstall notice
printf -- '\n%s\n' " Uninstalling your personal copy of Mozilla Thunderbird.";

## Install
rm -r -f "$HOME"/Mozilla/thunderbird32/ ;

# Menu shortcuts
rm -r -f "$HOMR"/.local/share/applications/Mozilla_Thunderbird32.desktop ;

# Desktop shotcuts

# determine the XDG_DESKTOP_DIR, for multi language support!
DESKTOP_DIR=$(xdg-user-dir DESKTOP)
# Current desktop shortcuts
rm -r -f "$DESKTOP_DIR"/Mozilla_Thunderbird32.desktop ;

# Exit notice
printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird." \
" Mozilla Thunderbird has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# Exit
exit 0
