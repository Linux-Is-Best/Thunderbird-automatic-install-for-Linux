#!/bin/sh
#
# Thunderbird automatic install for Linux - Personal uninstall - 32-bit
# v1.1.0

# Specify the directory where the Thunderbird installation scripts are located
SCRIPT_DIR="./personal32/uninstallers/"

# Function to install a single Thunderbird edition
install_single_edition() {
  edition="$1"
  script_path="${SCRIPT_DIR}/Thunderbird_${edition}.sh"


  if [ -f "$script_path" ]; then
    chmod +x "$script_path"  # Set the script as executable
    clear
    printf -- '\n%s\n\n' "You selected ${edition}"
    "$script_path"
  else
    clear
    printf -- '\n%s\n\n' "Script not found: $script_path"
  fi
}

# Function to uninstall all editions
uninstall_all_editions() {
  clear
  printf -- '\n%s\n\n' "You selected to uninstall ALL edition of Thunderbirds"
  chmod +x "${SCRIPT_DIR}/thunderbird_uninstall_all.sh"
  "${SCRIPT_DIR}/thunderbird_uninstall_all.sh"
}

# Function to PURGE all editions
purge_thunderbird() {
  clear
  printf -- '\n%s\n\n' "You selected to purge Thunderbird and ALL personal files. This cannot be undone!"
  printf "Are you sure you want to proceed? [y/n]: "
  read -r answer

  if [ "$answer" = "y" ] || [ "$answer" = "Y" ]; then
    chmod +x "${SCRIPT_DIR}/personal_purge.sh"
    "${SCRIPT_DIR}/personal_purge.sh"
  else
    printf -- '\n%s\n\n' "Purge operation cancelled. Thunderbird and personal files have not been removed."
  fi
}


# Function to display the main menu
display_main_menu() {
  clear
  printf -- '\n%s\n' " ";
  printf -- '%s\n' "   Thunderbird automatic install for Linux" \
    " " \
    "           P E R S O N A L - U N I N S T A L L - 3 2 b i t" \
    " " \
    " CAUTION - You are about to remove and delete" \
    "           Mozilla Thunderbird from your computer!" \
    " " \
    " 1. Mozilla Thunderbird" \
    " 2. Thunderbird Beta" \
    " 3. Thunderbird Daily" \
    " 4. Uninstall ALL editions of Thunderbird" \
    " 5. Purge Thunderbird and ALL personal files (Cannot be undone)" \
    " 6. Exit" \
    "" ""
  printf " Please enter option [1 - 6]: "
}

# Main script execution
while true; do
  display_main_menu
  read -r opt

  case $opt in
    1)
      install_single_edition "Stable"
      break
      ;;
    2)
      install_single_edition "Beta"
      break
      ;;
    3)
      install_single_edition "Daily"
      break
      ;;
    4)
      uninstall_all_editions
      break
      ;;
    5)
      purge_thunderbird
      break
      ;;
    6)
      clear
      printf -- '\n%s\n\n' "Goodbye, $USER"
      exit 0
      ;;
    *)
      clear
      printf -- '\n\n%s\n' " $opt is an invalid option. Please select an option between 1-6 only" \
        " Press the [enter] key to continue. . ."
      read -r  # Reading input without storing it in any variable
      ;;
  esac
done
