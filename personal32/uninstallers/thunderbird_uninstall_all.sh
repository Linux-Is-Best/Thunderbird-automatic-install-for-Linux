#!/bin/sh
#
# Thunderbird automatic install for Linux - all personal 32-bit - UNINSTALL
# v1.2.0

# Function for user confirmation
confirm_action() {
    printf "%s" "$1 [y/N]: "
    read -r confirm
    if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ]; then
        printf -- '%s\n' "" "" "Action canceled. No changes have been made." "" ""
        exit 0
    fi
}

# Prompt for user confirmation before uninstallation
confirm_action "Are you sure you want to uninstall all your personal editions of Mozilla Thunderbird - 32-bit?"

# Uninstall notice
printf -- '\n%s\n' "Uninstalling ALL your personal copies of Mozilla Thunderbird."

# Define the Thunderbird directories for different editions
THUNDERBIRD_DIR="$HOME/Mozilla/thunderbird32"
BETA_DIR="$HOME/Mozilla/thunderbird_beta32"
DAILY_DIR="$HOME/Mozilla/thunderbird_daily32"

# Uninstall each edition of Firefox
rm -r -f "$THUNDERBIRD_DIR"
rm -r -f "$BETA_DIR"
rm -r -f "$DAILY_DIR"

# Remove menu shortcuts
rm -r -f "$HOME/.local/share/applications/Mozilla_Thunderbird32.desktop"
rm -r -f "$HOME/.local/share/applications/Mozilla_Thunderbird_Beta32.desktop"
rm -r -f "$HOME/.local/share/applications/Mozilla_Thunderbird_Daily32.desktop"

# Remove desktop shortcuts
DESKTOP_DIR=$(xdg-user-dir DESKTOP)
rm -r -f "$DESKTOP_DIR/Mozilla_Thunderbird32.desktop"
rm -r -f "$DESKTOP_DIR/Mozilla_Thunderbird_Beta32.desktop"
rm -r -f "$DESKTOP_DIR/Mozilla_Thunderbird_Daily32.desktop"

# Exit notice
printf -- '%s\n' "" "" "" "Thank you for using Mozilla Thunderbird." \
"Thunderbird has been deleted and uninstalled. Per your request." \
"Really sorry to see you go. Hope to see you again real soon." "" ""

# Exit
exit 0
