#!/bin/sh
#
# Thunderbird automatic install for Linux - system uninstall Thunderbird - 32-bit
# v1.0.0
#

# Uninstall notice
printf -- '\n%s\n' " Uninstalling your 32-bit system copy of Mozilla Thunderbird.";

# 3 second delay to give user time to read the above notice
sleep 3;

# Installation
sudo rm -r -f /opt/thunderbird_32bit/ ;

# Menu shotcuts
sudo rm -r -f /usr/share/applications/Mozilla_Thunderbird32.desktop ;

# Desktop shortcuts
sudo rm -r -f /etc/skel/Desktop/Mozilla_Thunderbird32.desktop ;

# determine the XDG_DESKTOP_DIR, then the DESKTOP_NAME for multi language support, this assumes, that every user has the same locale or Desktop Name
DESKTOP_NAME=$(basename "$(sudo xdg-user-dir DESKTOP)")
# Current deskop shortcuts
sudo rm -r -f /home/*/"$DESKTOP_NAME"/Mozilla_Thunderbird32.desktop ;

## System PURGE - Everything go, bye-bye, for everyone!
# Uncomment to purge Cache files.
# sudo rm -r -f /home/*/.cache/thunderbird/ ;
# Uncomment to purge Configuration and profile files.
# sudo rm -r -f /home/*/.thunderbird/ ;

# exit notice
printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird." \
" Mozilla Thunderbird has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# exit
exit 0
