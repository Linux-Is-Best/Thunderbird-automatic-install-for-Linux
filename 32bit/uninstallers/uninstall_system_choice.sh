#!/bin/sh
#
# Thunderbird automatic install for Linux - System uninstall - 32-bit
# v1.1.0

# Specify the directory where the Thunderbird installation scripts are located
SCRIPT_DIR="./32bit/uninstallers"

# Function to install a single Thunderbird edition
install_single_edition() {
  edition="$1"
  script_path="${SCRIPT_DIR}/Thunderbird_${edition}.sh"


  if [ -f "$script_path" ]; then
    chmod +x "$script_path"  # Set the script as executable
    clear
    printf -- '\n%s\n\n' "You selected ${edition}"
    "$script_path"
  else
    clear
    printf -- '\n%s\n\n' "Script not found: $script_path"
  fi
}

# Function to uninstall all editions
uninstall_all_editions() {
  clear
  printf -- '\n%s\n\n' "You selected to uninstall ALL edition of Thunderbirds"
  chmod +x "${SCRIPT_DIR}/thunderbird_uninstall_all.sh"
  "${SCRIPT_DIR}/thunderbird_uninstall_all.sh"
}

# Function to purge all editions - Execute order 66
purge_all_editions() {
  clear
  printf -- '\n%s\n\n' "Caution:"
  printf -- '%s\n' "This will remove all 32-bit editions AND all browser cache and configuration files for all copies of Mozilla Thunderbird for all users on your computer (system-wide)."
  printf -- '%s\n' "Are you sure you want to proceed? (Y/N)"
  read -r confirm

  if [ "$confirm" = "Y" ] || [ "$confirm" = "y" ]; then
    chmod +x "${SCRIPT_DIR}/system_purge.sh"
    "${SCRIPT_DIR}/system_purge.sh"
  else
    printf -- '\n%s\n\n' "Cancelling the PURGE of ALL 32-bit editions."
  fi
}

# Function to display the main menu
display_main_menu() {
  clear
  printf -- '\n%s\n' " ";
  printf -- '%s\n' "   Thunderbird automatic install for Linux" \
    " " \
    "           S Y S T E M - U N I N S T A L L - 32 - b i t" \
    " " \
    " CAUTION - You are about to remove and delete" \
    "           Mozilla Thunderbird from your computer!" \
    " " \
    " 1. Mozilla Thunderbird" \
    " 2. Thunderbird Beta" \
    " 3. Thunderbird Daily" \
    " 4. Uninstall ALL editions of Thunderbird" \
    " 5. Exit" \
    " " \
    " 66. PURGE - CAUTION: Will remove all 32-bit editions" \
    "                     and all browser cache and configuration files for" \
    "                     all copies of Mozilla Thunderbird for all users on your computer." \
    "" ""
  printf " Please enter option [1 - 6, 66]: "
}

# Main script execution
while true; do
  display_main_menu
  read -r opt

  case $opt in
    1)
      install_single_edition "Stable"
      break
      ;;
    2)
      install_single_edition "Beta"
      break
      ;;
    3)
      install_single_edition "Daily"
      break
      ;;
    4)
      uninstall_all_editions
      break
      ;;
    5)
      clear
      printf -- '\n%s\n\n' "Goodbye, $USER"
      exit 0
      ;;
    66)
      purge_all_editions
      break
      ;;
    *)
      clear
      printf -- '\n\n%s\n' " $opt is an invalid option. Please select an option between 1-5 or 66 only" \
      " Press the [enter] key to continue. . ."
      read -r  # Reading input without storing it in any variable
      ;;
  esac
done
