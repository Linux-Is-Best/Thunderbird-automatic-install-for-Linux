#!/bin/sh
#
# Thunderbird Automatic Install for Linux -- PRUGE file -- Can be used independently.
#
# Purge - To remove all copies of Mozilla Thunderbird installed by this development
#         BUT will also remove ALL cache and configuration files for 
#         ALL copies of Mozilla Thunderbird for ALL users on your computer. 
#
# That which is done cannot be un-done. Re-installed, of course, but not un-done. 
#

# Purge Firefox notice
clear ;
printf -- '\n%s\n' " Per your request. Now purging Mozilla Thunderbird.";

# 3 second delay to give user time to read the above notice
sleep 3;

# Installation
sudo rm -r -f /opt/thunderbird_32bit/ ;
sudo rm -r -f /opt/thunderbird_beta32/ ;
sudo rm -r -f /opt/thunderbird_daily32/ ;

# Menu shotcuts
sudo rm -r -f /usr/share/applications/Mozilla_Thunderbird32.desktop ;
sudo rm -r -f /usr/share/applications/Mozilla_Thunderbird_Beta32.desktop ;
sudo rm -r -f /usr/share/applications/Mozilla_Thunderbird_Daily32.desktop ;


# Desktop shortcuts
sudo rm -r -f /etc/skel/Desktop/Mozilla_Thunderbird32.desktop ;
sudo rm -r -f /etc/skel/Desktop/Mozilla_Thunderbird_Beta32.desktop ;
sudo rm -r -f /etc/skel/Desktop/Mozilla_Thunderbird_Daily32.desktop ;

# determine the XDG_DESKTOP_DIR, then the DESKTOP_NAME for multi language support, this assumes, that every user has the same locale or Desktop Name
DESKTOP_NAME=$(basename "$(sudo xdg-user-dir DESKTOP)")
# Current deskop shortcuts
sudo rm -r -f /home/*/"$DESKTOP_NAME"/Mozilla_Thunderbird32.desktop ;
sudo rm -r -f /home/*/"$DESKTOP_NAME"/Mozilla_Thunderbird_Beta32.desktop ;
sudo rm -r -f /home/*/"$DESKTOP_NAME"/Mozilla_Thunderbird_Daily32.desktop ;

## System PURGE - Everything go, bye-bye, for everyone!

# Cache files - purge
sudo rm -r -f /home/*/.cache/thunderbird/ ;

# Configuration and profile files. - purge
sudo rm -r -f /home/*/.thunderbird/ ;

# exit notice
printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird." \
" Mozilla Thunderbird, along with all user data, have been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon" "" ""

# exit
exit 0
