#!/bin/sh
#
# Thunderbird automatic install for Linux - System install - 32-bit
# v1.1.0

# Specify the directory where the Thunderbird installation scripts are located
SCRIPT_DIR="./32bit"

# Function to install a single Thunderbird edition
install_single_edition() {
  edition="$1"
  script_path="${SCRIPT_DIR}/Thunderbird_${edition}.sh"

  if [ -f "$script_path" ]; then
    chmod +x "$script_path"  # Set the script as executable
    clear
    printf -- '\n%s\n\n' "You selected ${edition}"
    "$script_path"
  else
    clear
    printf -- '\n%s\n\n' "Script not found: $script_path"
  fi
}

# Function to install all 64-bit editions
install_all_editions() {
  clear
  printf -- '\n%s\n\n' "You selected to install ALL edition of Thunderbirds"
  chmod +x "${SCRIPT_DIR}/thunderbird_install_all.sh"
  "${SCRIPT_DIR}/thunderbird_install_all.sh"
}

# Function to display the main menu
display_main_menu() {
  clear
  printf -- '\n%s\n' " ";
  printf -- '%s\n' "   Thunderbird automatic install for Linux" \
    " " \
    "           S Y S T E M - I N S T A L L - 32 - b i t" \
    " " \
    " " \
    " 1. Mozilla Thunderbird" \
    " 2. Thunderbird Beta" \
    " 3. Thunderbird Daily" \
    " 4. Install ALL editions of Thunderbird" \
    " 5. Exit" \
    "" ""
  printf " Please enter option [1 - 5]: "
}

# Main script execution
while true; do
  display_main_menu
  read -r opt

  case $opt in
    1)
      install_single_edition "Stable"
      break
      ;;
    2)
      install_single_edition "Beta"
      break
      ;;
    3)
      install_single_edition "Daily"
      break
      ;;
    4)
      install_all_editions
      break
      ;;
    5)
      clear
      printf -- '\n%s\n\n' "Goodbye, $USER"
      exit 0
      ;;
    *)
      clear
      printf -- '\n\n%s\n' " $opt is an invalid option. Please select an option between 1-5 only" \
        " Press the [enter] key to continue. . ."
      read -r  # Reading input without storing it in any variable
      ;;
  esac
done
