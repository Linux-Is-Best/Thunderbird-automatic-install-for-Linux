#!/bin/sh
#
# Thunderbird automatic install for Linux - all system 64-bit - UNINSTALL
# v1.1.0
#

# Specify the directory where the Thunderbird uninstall scripts are located
SCRIPT_DIR="./64bit/uninstallers"

# Set executable permissions for all files in the ./uninstallers directory
chmod -R +x ./64bit/uninstallers

# List of edition names
EDITIONS="Stable Beta Daily"

# Function to install all editions
uninstall_all_editions() {
  printf -- '\n%s\n' "Now uninstalling all psystem 64-bit versions of Mozilla Thunderbird. This may take a moment, please wait..."
  sleep 2

  for edition in $EDITIONS; do
    script_path="${SCRIPT_DIR}/Thunderbird_${edition}.sh"
    if [ -f "$script_path" ]; then
      if [ -x "$script_path" ]; then
        "$script_path"
      else
        printf -- '\n%s\n' "Script not executable: $script_path"
      fi
    else
      printf -- '\n%s\n' "Script not found: $script_path"
    fi
  done
}

# Display an exit notice
display_exit_notice() {
  printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird." \
" Mozilla Thunderbird has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""
}

# Main script execution
uninstall_all_editions
display_exit_notice

# Exit
exit 0
