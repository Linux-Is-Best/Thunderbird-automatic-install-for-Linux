#!/bin/sh
#
# Thunderbird automatic install for Linux - system uninstall Thunderbird Daily - 64-bit
# v1.1.0
#

# Uninstall notice
printf -- '\n%s\n' " Uninstalling your 64-bit system copy of Mozilla Thunderbird Daily.";

# 3 second delay to give user time to read the above notice
sleep 3;

# Installation
sudo rm -r -f /opt/thunderbird_daily/ ;

# Menu shotcuts
sudo rm -r -f /usr/share/applications/Mozilla_Thunderbird_Daily.desktop ;

# Desktop shortcuts
sudo rm -r -f /etc/skel/Desktop/Mozilla_Thunderbird_Daily.desktop ;

# determine the XDG_DESKTOP_DIR, then the DESKTOP_NAME for multi language support, this assumes, that every user has the same locale or Desktop Name
DESKTOP_NAME=$(basename "$(sudo xdg-user-dir DESKTOP)")
# Current deskop shortcuts
sudo rm -r -f /home/*/"$DESKTOP_NAME"/Mozilla_Thunderbird_Daily.desktop ;

## System PURGE - Everything go, bye-bye, for everyone!
# Uncomment to purge Cache files.
# sudo rm -r -f /home/*/.cache/thunderbird/ ;
# Uncomment to purge Configuration and profile files.
# sudo rm -r -f /home/*/.thunderbird/ ;

# exit notice
printf -- '%s\n' "" "" "" " Thank you for using Mozilla Thunderbird Daily." \
" Mozilla Thunderbird Daily has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# exit
exit 0
